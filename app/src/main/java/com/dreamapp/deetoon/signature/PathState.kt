package com.dreamapp.deetoon.signature

import androidx.compose.ui.graphics.Path

data class PathState(
    val path: Path,
    val color: Int,
    val stroke: Float
)