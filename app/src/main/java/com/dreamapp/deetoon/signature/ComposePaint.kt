package com.dreamapp.deetoon.signature

import android.graphics.Color
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Path

@Composable
fun ComposePaint() {
    val paths = remember { mutableStateOf(mutableListOf<PathState>()) }
    Scaffold(
        topBar = {
            ComposePaintAppBar {
                paths.value = mutableListOf()
            }
        }
    ) {
        PaintBody(paths)
    }
}

@Composable
fun ComposePaintAppBar(onDelete: () -> Unit) {
    TopAppBar(
        title = {
            Text(text = "DeeToon")
        },
        actions = {
            IconButton(onClick = onDelete) {
                Icon(
                    imageVector = Icons.Default.Delete,
                    contentDescription = "Delete"
                )
            }
        }
    )
}

@Composable
fun PaintBody(paths: MutableState<MutableList<PathState>>) {
    Box(modifier = Modifier.fillMaxSize()) {
        val drawColor = remember { mutableStateOf(Color.BLACK) }
        val strokeBrush = remember { mutableStateOf(5f) }
        val usedColors =
            remember { mutableStateOf(mutableSetOf(Color.BLACK, Color.WHITE, Color.GRAY)) }

        paths.value.add(PathState(Path(), drawColor.value, strokeBrush.value))

        DrawingCanvas(
            drawColor,
            strokeBrush,
            usedColors,
            paths.value
        )
        /*
        DrawingTools(
            drawColor = drawColor,
            strokeBrush = strokeBrush,
            usedColors = usedColors.value
        )
         */
    }
}
/*
@Composable
fun DrawingTools(
    drawColor: MutableState<Int>,
    strokeBrush: MutableState<Float>,
    usedColors: MutableSet<Int>
) {
}
*/
